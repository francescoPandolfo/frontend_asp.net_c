﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using GNSShare_client.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace GNSShare_client.Controllers;

public class MappaController : Controller
{
    public string FORM_NON_VALIDO = "Il form non è compilato correttamente";
    //static public string previousAction = "";
    static public Dictionary<string, client.Stazione>? ListaStazioni;
    static public bool aggiuntaNuovaStazione = false;
    static public bool aggiuntoNuovoFile = false;
    static public string? SourceMappaStazioni;
    static public bool aggiornaMappaStazioni = true;

    public delegate void EventHandler(object sender, EventArgs e);
    public delegate Task<IActionResult> EventHandler_Controller(Controller ctr, object sender, System.Net.IPAddress? ip, EventArgs e);
    public delegate object? EventHandler_IP(object sender, System.Net.IPAddress? ip, EventArgs e);


    static public event EventHandler? InvocatoUploadFile;
    static public event EventHandler_IP? InvocatoGetElencoStazioni, InvocatoGetStazioni, InvocatoLoadFile, InvocatoGetMappaStazioni, InvocatoGetSingolaMappaStazione;
    static public event EventHandler_Controller? InvocatoDownloadFile;

    private readonly ILogger<MappaController> _logger;
    public static System.Net.IPAddress? remoteIpAddress;

    public MappaController(ILogger<MappaController> logger)
    {
        _logger = logger;
    }


    public IActionResult CaricaFile(CaricaFileModel model, EventArgs e)
    {
        List<client.Stazione>? elencoStazioni = (List<client.Stazione>?)InvocatoGetStazioni?.Invoke(model, Request.HttpContext.Connection.RemoteIpAddress, e);
        var stazioni = new List <SelectListItem> ();
        if (elencoStazioni is not null)
            foreach( client.Stazione elem in elencoStazioni)
                stazioni.Add(new SelectListItem {Value = elem.codice, Text = elem.codice});
        
        model.ListaStazioni = stazioni;

        ViewBag.ipAddress = Request.HttpContext.Connection.RemoteIpAddress;
        return View(model);  
    }

    public async Task<IActionResult> LoadFile(CaricaFileModel model, EventArgs e)
    {
        ViewBag.ipAddress = Request.HttpContext.Connection.RemoteIpAddress;
        try
        {
            ModelState.Remove("ListaStazioni"); //al ritorno dalla view questo attributo viene resettato, quindi lo escludo dalla validazione
            ModelState.Remove("nomeFile");
            ModelState.Remove("percorsoFile");
            if(ModelState.IsValid)
            {
                if( model.file == null || model.file.Length == 0) throw new Exception("File non corretto.");
            
                string? nomeFile = (string?)InvocatoLoadFile?.Invoke(model, Request.HttpContext.Connection.RemoteIpAddress, e);
                model.nomeFile = nomeFile;

                InvocatoUploadFile?.Invoke(model, e); //commentare quando saranno risolti i problemi del server di scrittura su file
                
                TempData["SuccessMessage"] = "Nuovo file caricato a sistema! Il nuovo file si chiama " + nomeFile;
                return RedirectToAction("CaricaFile");
            }
            throw new Exception(FORM_NON_VALIDO);
        }
        catch(Exception ex)
        {
            ExceptionHandler(ex.Message);
        }
        
        return RedirectToAction("CaricaFile", model);
    }

    public IActionResult MappaStazioni(MappaModel model, EventArgs e)
    {
        if(aggiornaMappaStazioni)
        {
            SourceMappaStazioni = (string?)InvocatoGetMappaStazioni?.Invoke(model, Request.HttpContext.Connection.RemoteIpAddress, e);
            aggiornaMappaStazioni = false;
        }
        ViewBag.MapStationsData = SourceMappaStazioni;
        
        //ViewBag.MapStationsData = SourceMappaStazioni;
        try
        {            
            if(ListaStazioni is null || ListaStazioni.Count() == 0 || aggiuntaNuovaStazione || aggiuntoNuovoFile) 
            {
                
                //SourceMappaStazioni = ViewBag.MapStationsData; //così al prossimo refresh la mappa non deve essere ricaricata

                ListaStazioni = new Dictionary<string, client.Stazione>();
                foreach( client.Stazione st in (List<client.Stazione>?)InvocatoGetElencoStazioni?.Invoke(model, Request.HttpContext.Connection.RemoteIpAddress, e))
                {
                    st.popolaElencoFileCaricati();
                    ListaStazioni.Add(st.codice, st);
                } 
            
                aggiuntaNuovaStazione = false;
                aggiuntoNuovoFile = false;
                
            }
            model.ListaStazioni = ListaStazioni;
            
            if(model.stazioneScelta is not null)
            {
                model._stazioneScelta = ListaStazioni[model.stazioneScelta];
                ViewBag.MapSingleStationData = InvocatoGetSingolaMappaStazione?.Invoke(model, Request.HttpContext.Connection.RemoteIpAddress, e);
            }

        }
        catch(Exception ex)
        {
            ExceptionHandler(ex.Message);
        }
        ViewBag.ipAddress = Request.HttpContext.Connection.RemoteIpAddress;
        return View(model);
    }

    public Task<IActionResult>? DownloadFile(MappaModel model, EventArgs e)
    { 
        ViewBag.ipAddress = Request.HttpContext.Connection.RemoteIpAddress;
        return InvocatoDownloadFile?.Invoke(this, model, Request.HttpContext.Connection.RemoteIpAddress, e);
    }

    public void ExceptionHandler(string message, string? page = null)
    {
        TempData["ErrorMessage"] = message;
        if(page is not null) Response.Redirect(page);
    }
}
