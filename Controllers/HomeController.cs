﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using GNSShare_client.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace GNSShare_client.Controllers;

public class HomeController : Controller
{
    public string FORM_NON_VALIDO = "Il form non è compilato correttamente";
    public string CHIAVE_DUPLICATA = "Il campo chiave(*) scelto è già presente a sistema";

    public delegate void EventHandler_Object(object sender, System.Net.IPAddress? ip, EventArgs e);
    public delegate object? EventHandler_IP(object sender, System.Net.IPAddress? ip, EventArgs e);

    static public event EventHandler_Object? InvocatoLogout, InvocatoLogin, InvocatoAddUser, InvocatoAddStation;
    static public event EventHandler_IP? InvocatoGetPlotStatistiche;

    private readonly ILogger<HomeController> _logger;
    public static System.Net.IPAddress? remoteIpAddress;

    public HomeController(ILogger<HomeController> logger)
    {
        _logger = logger;
    }

    public IActionResult Index()
    {  
        ViewBag.ipAddress = Request.HttpContext.Connection.RemoteIpAddress;
        return View();
    }

    public IActionResult AggiungiUtente(UtenteModel model)
    {

        var ruoli = new List <SelectListItem> ();
        foreach(GNSShare_client.client.Utente.Ruolo elem in Enum.GetValues(typeof(GNSShare_client.client.Utente.Ruolo)))
        if (elem != client.Utente.Ruolo.NOT_LOGGED)
            ruoli.Add(new SelectListItem { Value = ((int)elem).ToString(), Text = elem.ToString() });

        model.ListaRuoli = ruoli;

        ViewBag.ipAddress = Request.HttpContext.Connection.RemoteIpAddress;
        return View(model);
    }

    public IActionResult AggiungiStazione(StazioneModel model)
    {
        ViewBag.ipAddress = Request.HttpContext.Connection.RemoteIpAddress;
        return View(model);  
    }

    public IActionResult Login(LoginModel model)
    {
        ViewBag.ipAddress = Request.HttpContext.Connection.RemoteIpAddress;
        return View();
    }

    public void Logout(Object sender, EventArgs e)
    {
        ViewBag.ipAddress = Request.HttpContext.Connection.RemoteIpAddress;
        try{
            InvocatoLogout?.Invoke(sender, Request.HttpContext.Connection.RemoteIpAddress, e);

            TempData["SuccessMessage"] = "Logout effettuato con successo!";
            Response.Redirect("Index");
        }
        catch(Exception ex)
        {
            ExceptionHandler(ex.Message, "Index");
        } 
    }

    public IActionResult Statistiche(StatisticheModel model, EventArgs e)
    {
        model.tipoStatistica = "downloadPerStazione";
        ViewBag.plotTotalDownloads = InvocatoGetPlotStatistiche?.Invoke(model, Request.HttpContext.Connection.RemoteIpAddress, e);
        Thread.Sleep(200);
        model.tipoStatistica = "movimentiPerData";
        ViewBag.plotTotalMovements = InvocatoGetPlotStatistiche?.Invoke(model, Request.HttpContext.Connection.RemoteIpAddress, e);

        ViewBag.ipAddress = Request.HttpContext.Connection.RemoteIpAddress;
        return View(model);  
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }

    public IActionResult AddUser(Models.UtenteModel model, EventArgs e)
    {
        ViewBag.ipAddress = Request.HttpContext.Connection.RemoteIpAddress;
        try
        {
            ModelState.Remove("ListaRuoli");  //al ritorno dalla view questo attributo viene resettato, quindi lo escludo dalla validazione
            ModelState.Remove("nome");
            ModelState.Remove("cognome");
            ModelState.Remove("ente");
            if(ModelState.IsValid)
            {
                InvocatoAddUser?.Invoke(model, Request.HttpContext.Connection.RemoteIpAddress, e);
                TempData["SuccessMessage"] = "Utente inserito a sistema!";
                return RedirectToAction("AggiungiUtente");
            }
            throw new Exception(FORM_NON_VALIDO);
        }
        catch(Exception ex)
        {
            ExceptionHandler(ex.Message);
        }
        return RedirectToAction("AggiungiUtente", model);
    }

    public IActionResult AddStation(StazioneModel model, EventArgs e)
    {
        ViewBag.ipAddress = Request.HttpContext.Connection.RemoteIpAddress;
        try
        {
            ModelState.Remove("nome");
            ModelState.Remove("rete");
            ModelState.Remove("tipo");
            ModelState.Remove("gps");
            ModelState.Remove("glonass");
            ModelState.Remove("galileo");
            if(ModelState.IsValid)
            {
                model.codice = model.codice.ToUpper();
                InvocatoAddStation?.Invoke(model, Request.HttpContext.Connection.RemoteIpAddress, e);
                MappaController.aggiornaMappaStazioni = true;
                TempData["SuccessMessage"] = "Stazione inserita a sistema!";
                return RedirectToAction("AggiungiStazione");
            }
            throw new Exception(FORM_NON_VALIDO);
        }
        catch(Exception ex)
        {
            ExceptionHandler(ex.Message);
        }
        
        return RedirectToAction("AggiungiStazione", model);
    }

    public IActionResult DoLogin(LoginModel model, EventArgs e)
    {
        try
        {
            if(ModelState.IsValid)
            {
                InvocatoLogin?.Invoke(model, Request.HttpContext.Connection.RemoteIpAddress, e);
                TempData["SuccessMessage"] = "Login effettuato con successo!\nBenvenuto " + model.username;
                return RedirectToAction("Index");
            }
            else throw new Exception(FORM_NON_VALIDO);
        }
        catch(Exception ex)
        {
            ExceptionHandler(ex.Message);
        }
        return RedirectToAction("Login", model);
    }   

    public void ExceptionHandler(string message, string? page = null)
    {
        if(message.StartsWith("SQL01")) TempData["ErrorMessage"] = CHIAVE_DUPLICATA + " [" + message + "]";
        else TempData["ErrorMessage"] = message;
        if(page is not null) Response.Redirect(page);
    }
}
