
using System;  
using System.Net;
using System.Net.Sockets;  
using System.Text; 

namespace GNSShare_client.server.DTP;

public class SocketTCP : server.IDTP
{
    static private Socket sock; //deve essere statico perchè il client deve utilizzare sempre lo stesso socket già aperto
    private int bufferDimension = 1000000;//2048000;
    private string SERVER_CPP_IP = "127.0.0.1";
    private int SERVER_CPP_PORT = 8080;

    public string InviaRichiestaAlServer(string msg, bool retriveImage = false, byte[]? messageToSendByte = null)
    {
        Console.WriteLine(IsConnected());
        if(!IsConnected()) return StartClient(msg, retriveImage, messageToSendByte);
        else return SendRequest(msg, retriveImage, messageToSendByte);
    }
    private string StartClient(string messageToSend, bool retriveImage = false, byte[]? messageToSendByte = null) 
    {  
         string toReturn = "ERRORE";
  
        // Connect to a remote device.  
        try {
            IPAddress ipAddress = IPAddress.Parse(SERVER_CPP_IP);
            IPEndPoint remoteEP = new IPEndPoint(ipAddress,SERVER_CPP_PORT);  
  
            // Create a TCP/IP  socket.  
            //Socket sender = new Socket(ipAddress.AddressFamily,
            sock = new Socket(ipAddress.AddressFamily,
                SocketType.Stream, ProtocolType.Tcp );  
  
            // Connect the socket to the remote endpoint. Catch any errors.  
            try {  
                sock.Connect(remoteEP);  
  
                Console.WriteLine("Socket connected to {0}",  
                    sock.RemoteEndPoint.ToString());  
  
                // Data buffer for incoming data. 
                byte[] bytes = new byte[bufferDimension];  
                // Encode the data string into a byte array.  
                byte[] msg = new byte[bufferDimension];

                msg = Encoding.ASCII.GetBytes(messageToSend);

                if(messageToSendByte != null) msg = msg.Concat(messageToSendByte).ToArray();
  
                // Send the data through the socket.  
                int bytesSent = sock.Send(msg);  
                Console.WriteLine("  -->>  " + messageToSend);
  
                // Receive the response from the remote device. 
                sock.ReceiveTimeout = 50000; 
                sock.ReceiveBufferSize = bufferDimension;
                Thread.Sleep(400);
                int bytesRec = sock.Receive(bytes);  
                Console.WriteLine("  <<--  {0}", Encoding.ASCII.GetString(bytes,0,bytesRec));  
                Console.WriteLine("Connessione riuscita!");

                if(!retriveImage) toReturn = Encoding.ASCII.GetString(bytes,0,bytesRec);
                else
                {
                    byte[] myImage = bytes.Take(bytesRec).ToArray();
                    //byte[] bytesToRender = myImage.Skip(221).ToArray();

                    string imreBase64Data = Convert.ToBase64String(myImage);
                    toReturn = string.Format("data:image/png;base64,{0}", imreBase64Data);
                }
  
            } catch (ArgumentNullException ane) {  
                Console.WriteLine("ArgumentNullException : {0}",ane.ToString());  
            } catch (SocketException se) {  
                Console.WriteLine("SocketException : {0}",se.ToString());  
                sock.Close();
            } catch (Exception e) {  
                Console.WriteLine("Unexpected exception : {0}", e.ToString());  
            }  
  
        } catch (Exception e) {  
            Console.WriteLine( e.ToString());  
        } 
        return toReturn;
    }  

    private string SendRequest(string messageToSend, bool retriveImage = false, byte[]? messageToSendByte = null)
    {
        string toReturn = "ERRORE";
        //Console.WriteLine("Sto inviando una nuova richiesta");
        if(sock!=null){
            try { 
                byte[] bytes = new byte[bufferDimension];
                // Encode the data string into a byte array.  
                byte[] msg = new byte[bufferDimension]; 
                msg = Encoding.ASCII.GetBytes(messageToSend);

                
                
                // Send the data through the socket.  
                int bytesSent = sock.Send(msg);
                Console.WriteLine("  -->>  " + messageToSend);
                if(messageToSendByte != null)
                {
                    Thread.Sleep(200);
                    sock.Send(messageToSendByte);
                    //aggiungo il byte di fine file
                    Thread.Sleep(200);
                    sock.Send(Encoding.ASCII.GetBytes("#FINE-FILE#"));
                }

                // Receive the response from the remote device. 
                sock.ReceiveTimeout = 50000; 
                sock.ReceiveBufferSize = bufferDimension;
                Thread.Sleep(400);
                int bytesRec = sock.Receive(bytes);  
                Console.WriteLine("  <<--  {0}", Encoding.ASCII.GetString(bytes,0,bytesRec));  

                if(!retriveImage) toReturn = Encoding.ASCII.GetString(bytes,0,bytesRec);
                else
                {
                    byte[] myImage = bytes.Take(bytesRec).ToArray();
                    //byte[] bytesToRender = myImage.Skip(221).ToArray();

                    string imreBase64Data = Convert.ToBase64String(myImage);
                    toReturn = string.Format("data:image/png;base64,{0}", imreBase64Data);
                }
                
            } catch (ArgumentNullException ane) {  
                Console.WriteLine("ArgumentNullException : {0}",ane.ToString());  
            } catch (SocketException se) {  
                Console.WriteLine("SocketException : {0}",se.ToString());  
            } catch (Exception e) {  
                Console.WriteLine("Unexpected exception : {0}", e.ToString());  
            }
        }
        return toReturn;
    }

    private bool IsConnected()
    {
        Console.WriteLine(sock);
        if(sock == null || !sock.IsBound) return false;
        return sock.Connected && !(sock.Poll(1, SelectMode.SelectRead) && sock.Available == 0);
    }
}

    