/*DTP: Data Transfer Process
Interfaccia che definisce i metodi per il processo di trasferimento dei dati al Server,
implementa il metodo di default utilizzato per inviare le richieste al server.
Per poter essere utilizzata la classe derivata deve fornire l'oggetto su cui eseguire le operazioni volute.
Implementa il pattern "Factory Method".
*/

namespace GNSShare_client.server;

interface IDTPFactory
{
    server.IDTP factoryDTP();
    
    public string InviaRichiesta_DTP(string messageToSend, bool retriveImage = false, byte[]? messageToSendByte = null){ //è dichiarato protected perchè deve essere visibile solo all'interno della derivata, non da istanze della derivata
        server.IDTP myDTP = factoryDTP();
        return myDTP.InviaRichiestaAlServer(messageToSend, retriveImage, messageToSendByte);
    }
}