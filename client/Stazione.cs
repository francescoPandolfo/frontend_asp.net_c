using Microsoft.AspNetCore.Mvc.Rendering;
namespace GNSShare_client.client;

public class Stazione
{

    public class File
    {
        public string nome, dataCreazione, inizioPeriodo, finePeriodo;
        public float frequenza;
    }

    public string codice;
    public string? nome, rete, tipo;
    public float latitudine, longitudine, altitudine;
    public bool gps, galileo, glonass;
    public List<File>? fileCaricati;
    public List<SelectListItem> elencoFileCaricati;
    public string fileScelto;

    public Stazione(string codice, string nome, float lat, float lng, float alt, string rete, string tipo, bool gps, bool gal, bool glo, List<File>? fileCaricati)
    {
        this.codice         = codice;
        this.nome           = nome;
        this.latitudine     = lat;
        this.longitudine    = lng;
        this.altitudine     = alt;
        this.rete           = rete;
        this.tipo           = tipo;
        this.gps            = gps;
        this.galileo        = gal;
        this.glonass        = glo;
        this.fileCaricati   = fileCaricati;

        popolaElencoFileCaricati();
    }

    public Stazione(){}

    public string toString(){
        return codice + " (" + nome + ")";
    }    

    public void popolaElencoFileCaricati(){
        this.elencoFileCaricati = new List<SelectListItem>();
        
        if(this.fileCaricati is not null)
        {
            foreach(File elem in this.fileCaricati)
                this.elencoFileCaricati.Add(new SelectListItem { Value = elem.nome, Text = elem.nome });
        }
    }
}