namespace GNSShare_client.client;

public class Utente
{

    public enum Ruolo{ ADMIN, EDITOR, VIEWER, NOT_LOGGED }; //assicurarsi che NOT_LOGGED stia sempre alla fine
    public string username, password;
    public string? nome, cognome, ente;
    public Ruolo ruolo = Ruolo.NOT_LOGGED;
    public List<int> endpoints; //elenco delle operazioni accessibili dall'utente in base al suo ruolo

    public Utente(string user, string paswd, string nome, string cognome, string ente, Ruolo ruolo, List<int> endp)
    {
        this.username = user;
        this.password = paswd;
        this.nome = nome;
        this.cognome = cognome;
        this.ente = ente;
        this.ruolo = ruolo;
        this.endpoints = endp;
    }

    public Utente(){}

    public string toString(){
        return nome + " " + cognome + " (" + ente + ")";
    }

    
}