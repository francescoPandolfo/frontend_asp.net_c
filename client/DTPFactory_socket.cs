using Newtonsoft.Json;
using System.Net;

namespace GNSShare_client.client;

public class DTPFactory_socket : server.IDTPFactory
{
    static public long ipHostAddress; //in modo che sia visibile alla struct SendToServer
        
    public struct SendToServer{
        public long ipClientAddress;
        public string username, password;
        public GNSShare.Operazioni operazione;

        public Dictionary<string, string>? parametri;

        public SendToServer(string u, string p, GNSShare.Operazioni o, Dictionary<string, string>? par = null )
        {
            this.ipClientAddress  = ipHostAddress;
            this.username       = u;
            this.password       = p;
            this.operazione     = o;
            this.parametri      = par;
        }
    }

    public DTPFactory_socket(long ipAddress){
        ipHostAddress = ipAddress;
    }

    public server.IDTP factoryDTP(){
        server.IDTP toReturn = new server.DTP.SocketTCP();
        return toReturn; 
    }

    public void InviaRichiesta(string username, string password, GNSShare.Operazioni operazione, out Object respObj, Dictionary<string, string>? listaParametri = null, bool retriveImage = false, byte[]? fileStream = null)
    {
        SendToServer messageToSend = new SendToServer(username, password, operazione, listaParametri);

        /*una derivata di un'interfaccia non può chiamare direttamente METODI DI DEFAULT dell'interfaccia,
        questi sono fruibili solo da istanze di essa, quindi per accedervi dobbiamo fare prima un cast a partire dalla derivata corrente (this)
        L'alternativa sarebbe stata definire IDTPFactory come classe astratta, in tal caso ci avremmo potuto accedere tramite la keyword "base"*/

        string msgFromServer = ((server.IDTPFactory)this).InviaRichiesta_DTP( JsonConvert.SerializeObject(messageToSend), retriveImage, fileStream);
        if(msgFromServer!="ERRORE"){
            switch(operazione)
            {
                case GNSShare.Operazioni.LOGIN:
                case GNSShare.Operazioni.LOGOUT:
                    respObj = JsonConvert.DeserializeObject<GNSShare.objectResponseLogin>(msgFromServer);
                    GNSShare.objectResponseLogin _resp = (GNSShare.objectResponseLogin)respObj;
                    if(_resp.isAuthenticated) _resp.user.endpoints = _resp.endpoints; //gli endpoints li aggiungo come attributo dell'utente
                    respObj = _resp;
                    break;
                case GNSShare.Operazioni.CARICA_FILE:
                    respObj = JsonConvert.DeserializeObject<GNSShare.objectResponseLoadFile>(msgFromServer);
                    break;
                case GNSShare.Operazioni.GET_STAZIONI:
                    respObj = JsonConvert.DeserializeObject<GNSShare.objectResponseGetStazioni>(msgFromServer);                
                    break;
                case GNSShare.Operazioni.GET_MAPPA_STAZIONI:
                case GNSShare.Operazioni.STATISTICHE:
                    GNSShare.objectResponseGeneric resp = new GNSShare.objectResponseGeneric();
                    resp.operationCompleted = true;
                    resp.serverReply = msgFromServer;
                    respObj = (Object)resp;
                    break;
                case GNSShare.Operazioni.DOWNLOAD_FILE:
                case GNSShare.Operazioni.AGGIUNGI_UTENTE:
                case GNSShare.Operazioni.AGGIUNGI_STAZIONE:
                default:
                    respObj = JsonConvert.DeserializeObject<GNSShare.objectResponseGeneric>(msgFromServer);
                    break;
            } 
        }
        else throw new Exception("Il server è andato in errore e non ha prodotto nessuna risposta.");
    }

}
