using Newtonsoft.Json;
using System.Net;

using Microsoft.AspNetCore.Mvc;
namespace GNSShare_client.client;

public class GNSShare
{
    static public string NO_IP_CLIENT = "Impossibile determinare l'IP del client";
    public enum Operazioni{ AGGIUNGI_UTENTE, AGGIUNGI_STAZIONE, CARICA_FILE, ELENCO_STAZIONI, ELENCO_UTENTI, LOGIN, LOGOUT, ESCI, GET_STAZIONI, GET_MAPPA_STAZIONI, REFRESH_MAPPA_STAZIONI, DOWNLOAD_FILE, STATISTICHE };

    static private Dictionary<long, Utente> utentiConnessi;
    static public Dictionary<string, Stazione> elencoStazioni;
    static public int DIM_BUFFER_READ = 10000000;

    public class objectResponseGeneric
    {
        public bool operationCompleted;
        public string? serverReply;
    }

    public class objectResponseLogin : objectResponseGeneric
    {
        public bool isAuthenticated;
        public int role;
        public List<int>? endpoints; // = new List<int>{5};
        public Utente? user;
    }

    public class objectResponseLoadFile : objectResponseGeneric
    {
        public string? nomeFile;
    }
    
    public class objectResponseGetStazioni : objectResponseGeneric
    {
        public List<Stazione> elencoStazioni;
    }

//costruttore

    public GNSShare()
    {
        Console.WriteLine("Chiamato costruttore GNSShare");
        Controllers.HomeController.InvocatoLogout += Logout;
        Controllers.HomeController.InvocatoLogin += Login;
        Controllers.HomeController.InvocatoAddUser += AddUser;
        Controllers.HomeController.InvocatoAddStation += AddStation;
        Controllers.HomeController.InvocatoGetPlotStatistiche += GetPlotStatistiche;

        Controllers.MappaController.InvocatoGetStazioni += GetStations;
        Controllers.MappaController.InvocatoGetMappaStazioni += GetStationsMap;
        Controllers.MappaController.InvocatoGetSingolaMappaStazione += GetSingleStationMap;

        Controllers.MappaController.InvocatoLoadFile += LoadFile;
        Controllers.MappaController.InvocatoGetElencoStazioni += GetStationsAll;
        Controllers.MappaController.InvocatoDownloadFile += DownloadFile;
        Controllers.MappaController.InvocatoUploadFile += UploadFile;

        utentiConnessi = new Dictionary<long, Utente>();
    }

    static public bool isAuthenticated(System.Net.IPAddress ip)
    {
        return utentiConnessi is not null && utentiConnessi.ContainsKey(ip.Address);
    }

    static public List<int> endpoints(System.Net.IPAddress ip)
    {
        if ( utentiConnessi is not null && utentiConnessi.ContainsKey(ip.Address) )
            return utentiConnessi[ip.Address].endpoints;
        else return new List<int>{(int)Operazioni.ELENCO_STAZIONI, (int)Operazioni.LOGIN}; //chi non si autentica può solo vedere la mappa delle stazioni o fare il login
    }

    static public Utente utenteCorrente(System.Net.IPAddress ip)
    {
        if ( utentiConnessi is not null && utentiConnessi.ContainsKey(ip.Address) )
            return utentiConnessi[ip.Address];
        else return new Utente();
    }

    static public Utente getUserFromIpAddress(System.Net.IPAddress? ip)
    {
        Utente toReturn;
        if(ip is not null) toReturn = utentiConnessi.ContainsKey(ip.Address) ? utentiConnessi[ip.Address] :  new Utente();
        else toReturn = new Utente();

        return toReturn;
    }

    static private void AddUser(Object s, System.Net.IPAddress? ip, EventArgs e)
    {
        if (ip is null) throw new Exception(NO_IP_CLIENT);
        DTPFactory_socket myDTP = new DTPFactory_socket(ip.Address);
        Object _resp;
        Models.UtenteModel sender = (Models.UtenteModel)s;
        Dictionary<string, string> parametri = new Dictionary<string, string>();
        parametri.Add("username",   sender.username);
        parametri.Add("password",   sender.password);
        parametri.Add("nome",       sender.nome);
        parametri.Add("cognome",    sender.cognome);
        parametri.Add("ente",       sender.ente);
        parametri.Add("ruolo",      sender.ruolo.ToString()); 

        //estraggo l'utente connesso in base al'ip del client che invia la richiesta
        Utente utenteCorrente = getUserFromIpAddress(ip);

        myDTP.InviaRichiesta(utenteCorrente.username, 
                            utenteCorrente.password, 
                            Operazioni.AGGIUNGI_UTENTE, 
                            out _resp,
                            parametri);
        
        objectResponseGeneric resp = (objectResponseGeneric) _resp;
        if(!resp.operationCompleted) throw new Exception(resp.serverReply);
    }

    static private void AddStation(Object s, System.Net.IPAddress? ip, EventArgs e)
    {
        if (ip is null) throw new Exception(NO_IP_CLIENT);
        DTPFactory_socket myDTP = new DTPFactory_socket(ip.Address);
        Object _resp;
        Models.StazioneModel sender = (Models.StazioneModel)s;
        Dictionary<string, string> parametri = new Dictionary<string, string>();
        parametri.Add("codice",     sender.codice);
        parametri.Add("nome",       sender.nome);
        parametri.Add("latitudine", sender.latitudine.ToString().Replace(",", "."));
        parametri.Add("longitudine", sender.longitudine.ToString().Replace(",", "."));
        parametri.Add("altitudine", sender.altitudine.ToString().Replace(",", "."));
        parametri.Add("rete",       sender.rete); 
        parametri.Add("tipo",       sender.tipo); 
        parametri.Add("gps",        sender.gps.ToString()); 
        parametri.Add("glonass",    sender.glonass.ToString()); 
        parametri.Add("galileo",    sender.galileo.ToString()); 

        //estraggo l'utente connesso in base al'ip del client che invia la richiesta
        Utente utenteCorrente = getUserFromIpAddress(ip);

        myDTP.InviaRichiesta(utenteCorrente.username, 
                            utenteCorrente.password, 
                            Operazioni.AGGIUNGI_STAZIONE, 
                            out _resp,
                            parametri);
        
        objectResponseGeneric resp = (objectResponseGeneric) _resp;
        if(!resp.operationCompleted) throw new Exception(resp.serverReply);

        Controllers.MappaController.aggiuntaNuovaStazione = true;

        //aggiorno la mappa dato che è presente una nuova stazione
        myDTP.InviaRichiesta(utenteCorrente.username, 
                            utenteCorrente.password, 
                            Operazioni.REFRESH_MAPPA_STAZIONI, 
                            out _resp);
        resp = (objectResponseGeneric) _resp;
        if(!resp.operationCompleted) throw new Exception(resp.serverReply);
    }

    static private string? LoadFile(Object s, System.Net.IPAddress? ip, EventArgs e)
    {
        if (ip is null) throw new Exception(NO_IP_CLIENT);

        Models.CaricaFileModel model = (Models.CaricaFileModel)s;
        /*legge il file e lo assegna all'array di byte (buffer)*/
   
        byte[] readerBuffer = new byte[DIM_BUFFER_READ];
        Stream myStream = model.file.OpenReadStream();
        int byteLetti = myStream.Read(readerBuffer, 0, DIM_BUFFER_READ);
        byte[] MsgToSend = readerBuffer.Take(byteLetti).ToArray();
        
        DTPFactory_socket myDTP = new DTPFactory_socket(ip.Address);
        Object _resp;
        Utente utenteCorrente = getUserFromIpAddress(ip);

        Dictionary<string, string> parametri = new Dictionary<string, string>();
        parametri.Add("codiceStazione", model.codiceStazione);
        parametri.Add("dataCreazione",  model.dataCreazione);
        parametri.Add("frequenza",      model.frequenza.ToString().Replace(",", "."));
        parametri.Add("inizioPeriodo",  model.inizioPeriodo);
        parametri.Add("finePeriodo",    model.finePeriodo);
        parametri.Add("estensione",     model.file.FileName.Split(".")[1]);

        myDTP.InviaRichiesta(utenteCorrente.username, 
                            utenteCorrente.password, 
                            Operazioni.CARICA_FILE, 
                            out _resp,
                            parametri,
                            false
                            /*,MsgToSend*/); //includere il parametro commentato quando saranno risolti i problemi di scrittura su file
        
        objectResponseLoadFile resp = (objectResponseLoadFile) _resp;
        if(!resp.operationCompleted) throw new Exception(resp.serverReply);
        Controllers.MappaController.aggiuntoNuovoFile = true;
        return resp.nomeFile;
    }

    static private List<Stazione>? GetStations(Object s, System.Net.IPAddress? ip, EventArgs e)
    {
        if (ip is null) throw new Exception(NO_IP_CLIENT);
        DTPFactory_socket myDTP = new DTPFactory_socket(ip.Address);
        Object _resp;

        Dictionary<string, string> parametri = new Dictionary<string, string>();
        parametri.Add("kv", "1"); //estrae solo chiave e valore (descrizione)

        //estraggo l'utente connesso in base al'ip del client che invia la richiesta
        Utente utenteCorrente = getUserFromIpAddress(ip);

        myDTP.InviaRichiesta(utenteCorrente.username, 
                            utenteCorrente.password, 
                            Operazioni.GET_STAZIONI, 
                            out _resp,
                            parametri);
        
        objectResponseGetStazioni resp = (objectResponseGetStazioni) _resp;
        if(!resp.operationCompleted) throw new Exception(resp.serverReply);
        return resp.elencoStazioni;
    }

    static private List<Stazione>? GetStationsAll(Object s, System.Net.IPAddress? ip, EventArgs e)
    {
        if (ip is null) throw new Exception(NO_IP_CLIENT);
        DTPFactory_socket myDTP = new DTPFactory_socket(ip.Address);
        Object _resp;

        //estraggo l'utente connesso in base al'ip del client che invia la richiesta
        Utente utenteCorrente = getUserFromIpAddress(ip);

        myDTP.InviaRichiesta(utenteCorrente.username, 
                            utenteCorrente.password, 
                            Operazioni.GET_STAZIONI, 
                            out _resp);
        
        objectResponseGetStazioni resp = (objectResponseGetStazioni) _resp;
        if(!resp.operationCompleted) throw new Exception(resp.serverReply);
        return resp.elencoStazioni;
    }

    static private void Login(Object s, System.Net.IPAddress? ip, EventArgs e)
    {
        if (ip is null) throw new Exception(NO_IP_CLIENT);
        DTPFactory_socket myDTP = new DTPFactory_socket(ip.Address);
        Object _resp;
        Models.LoginModel ss = (Models.LoginModel)s;

        myDTP.InviaRichiesta(ss.username, ss.password, Operazioni.LOGIN, out _resp);
        objectResponseLogin resp = (objectResponseLogin)_resp;

        if(resp.isAuthenticated)
        {
            utentiConnessi[ip.Address] = resp.user;
            //username e password non vengono restituite dal server (per sicurezza)
            utentiConnessi[ip.Address].username = ss.username;
            utentiConnessi[ip.Address].password = ss.password;
        }
        
        if(!resp.operationCompleted) throw new Exception(resp.serverReply);
        
    }

    static public void Logout(object s, System.Net.IPAddress? ip, EventArgs e)
    {
        if (ip is null) throw new Exception(NO_IP_CLIENT);
        DTPFactory_socket myDTP = new DTPFactory_socket(ip.Address);
        Object _resp;

        //estraggo l'utente connesso in base al'ip del client che invia la richiesta
        Utente utenteCorrente = getUserFromIpAddress(ip);

        myDTP.InviaRichiesta(utenteCorrente.username, utenteCorrente.password, Operazioni.LOGOUT, out _resp);
        objectResponseLogin resp = (objectResponseLogin)_resp;
        utentiConnessi.Remove(ip.Address);
    }

    static private async void UploadFile(Object s, EventArgs e)
    {
        Models.CaricaFileModel model = (Models.CaricaFileModel)s;

        var dir = Path.Combine(Directory.GetCurrentDirectory(), "Repository", model.codiceStazione );
        var percorsoFile = Path.Combine(dir, model.nomeFile);
        model.percorsoFile = percorsoFile;

        if(!File.Exists(dir)) Directory.CreateDirectory(dir);
        
        using (var fileStream = new FileStream(percorsoFile, FileMode.Create))
        {
            await model.file.CopyToAsync(fileStream);
        }
    }

    static private async Task<IActionResult> DownloadFile(Controller ctr, Object s, System.Net.IPAddress? ip, EventArgs e)
    {
        Models.MappaModel model = (Models.MappaModel)s;

        //Aggiorno il record del database incrementando di 1 i download del file scelto

        if (ip is null) throw new Exception(NO_IP_CLIENT);
        DTPFactory_socket myDTP = new DTPFactory_socket(ip.Address);
        Object _resp;
        Utente utenteCorrente = getUserFromIpAddress(ip);
        Dictionary<string, string> parametri = new Dictionary<string, string>();
        parametri.Add("codiceStazione", model.stazioneScelta);
        parametri.Add("nomeFile", model.fileScelto);

        myDTP.InviaRichiesta(utenteCorrente.username, 
                            utenteCorrente.password, 
                            Operazioni.DOWNLOAD_FILE, 
                            out _resp,
                            parametri);
        
        objectResponseGeneric resp = (objectResponseGeneric) _resp;
        if(!resp.operationCompleted) throw new Exception(resp.serverReply);

        //scarico fisicamente il file

        var filePath = Path.Combine(Directory.GetCurrentDirectory(),"Repository", model.stazioneScelta, model.fileScelto);

        var memoryStream = new MemoryStream();

        using (var stream = new FileStream(filePath, FileMode.Open))
        {
            await stream.CopyToAsync(memoryStream);            
        }
        // set the position to return the file from
        memoryStream.Position = 0;

        var mimeType = (string file) =>
        {            
            var mimeTypes = new Dictionary<string, string>(){ {".21o", "text/plain" }, {".21d", "text/plain" } };

            var extension = Path.GetExtension(file).ToLowerInvariant();
            return mimeTypes[extension];
        };

        return ctr.File(memoryStream, mimeType(filePath), Path.GetFileName(filePath));      
    }

    static private string? GetPlotStatistiche(Object s, System.Net.IPAddress? ip, EventArgs e)
    {
        if (ip is null) throw new Exception(NO_IP_CLIENT);
        DTPFactory_socket myDTP = new DTPFactory_socket(ip.Address);
        Object _resp;
        Models.StatisticheModel model = (Models.StatisticheModel)s;

        //estraggo l'utente connesso in base al'ip del client che invia la richiesta
        Utente utenteCorrente = getUserFromIpAddress(ip);
        Dictionary<string, string> parametri = new Dictionary<string, string>();
        parametri.Add("tipoStatistica", model.tipoStatistica);

        myDTP.InviaRichiesta(utenteCorrente.username, 
                            utenteCorrente.password, 
                            Operazioni.STATISTICHE, 
                            out _resp,
                            parametri,
                            true);
        
        objectResponseGeneric resp = (objectResponseGeneric)_resp;

        if(!resp.operationCompleted) throw new Exception(resp.serverReply);
        return resp.serverReply;
    }

    static private string? GetStationsMap(Object s, System.Net.IPAddress? ip, EventArgs e)
    {
        if (ip is null) throw new Exception(NO_IP_CLIENT);
        DTPFactory_socket myDTP = new DTPFactory_socket(ip.Address);
        Object _resp;

        //estraggo l'utente connesso in base al'ip del client che invia la richiesta
        Utente utenteCorrente = getUserFromIpAddress(ip);

        myDTP.InviaRichiesta(utenteCorrente.username, 
                            utenteCorrente.password, 
                            Operazioni.GET_MAPPA_STAZIONI, 
                            out _resp,
                            null,
                            true);
        
        objectResponseGeneric resp = (objectResponseGeneric)_resp;

        if(!resp.operationCompleted) throw new Exception(resp.serverReply);
        return resp.serverReply;

        /* invia una GET direttamente al modulo Python*/
        /*
        HttpWebRequest httpRequest = (HttpWebRequest) WebRequest.Create("http://10.200.101.177:5000/plots/europemap" );
        httpRequest.Method = "GET";
        //httpRequest.ContentType = "application/x-www-form-urlencoded";

        HttpWebResponse httpResponse = (HttpWebResponse) httpRequest.GetResponse();
        // Retrieve the response stream
        BinaryReader httpResponseStream = new BinaryReader(httpResponse.GetResponseStream(), System.Text.Encoding.UTF8);

        byte [ ] readData;
        string imreBase64Data;
        int byteToRead = (int)httpResponse.ContentLength;
        while ( true )
        {
            readData = httpResponseStream.ReadBytes( byteToRead );
            imreBase64Data = Convert.ToBase64String(readData);
            break;

            //if ( readData.Length == 0 )break;
        }

        httpResponseStream.Close();
        httpResponse.Close();
        return string.Format("data:image/png;base64,{0}", imreBase64Data);
        */
    }
    static private string? GetSingleStationMap(Object s, System.Net.IPAddress? ip, EventArgs e)
    {
        if (ip is null) throw new Exception(NO_IP_CLIENT);
        DTPFactory_socket myDTP = new DTPFactory_socket(ip.Address);
        Object _resp;

        //estraggo l'utente connesso in base al'ip del client che invia la richiesta
        Utente utenteCorrente = getUserFromIpAddress(ip);
        Models.MappaModel sender = (Models.MappaModel)s;
        Dictionary<string, string> parametri = new Dictionary<string, string>();
        parametri.Add("codiceStazione", sender.stazioneScelta);
        parametri.Add("latitudine", sender.ListaStazioni[sender.stazioneScelta].latitudine.ToString().Replace(",","."));
        parametri.Add("longitudine", sender.ListaStazioni[sender.stazioneScelta].longitudine.ToString().Replace(",","."));

        myDTP.InviaRichiesta(utenteCorrente.username, 
                            utenteCorrente.password, 
                            Operazioni.GET_MAPPA_STAZIONI, 
                            out _resp,
                            parametri,
                            true);
        
        objectResponseGeneric resp = (objectResponseGeneric)_resp;

        if(!resp.operationCompleted) throw new Exception(resp.serverReply);
        return resp.serverReply;
    }

}
