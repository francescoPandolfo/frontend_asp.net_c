using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;
namespace GNSShare_client.Models;

public class CaricaFileModel
{
    public List<SelectListItem> ListaStazioni { get; set; }
    [Required]
    public string codiceStazione { get; set; }
    
    [RegularExpression(@"^\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2}$")]
    [Required]
    public string dataCreazione { get; set; }

    [Required]
    [RegularExpression(@"^[0-9]*(?:[\.,][0-9]*)?$")]
    public string frequenza { get; set; }

    [RegularExpression(@"^\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2}$")]
    [Required]
    public string inizioPeriodo { get; set; }

    [RegularExpression(@"^\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2}$")]
    [Required]
    public string finePeriodo { get; set; }

    public string nomeFile { get; set; }
    public string percorsoFile { get; set; }

    [Required]
    public IFormFile file { get; set; }
}
