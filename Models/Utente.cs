using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;
namespace GNSShare_client.Models;

public class UtenteModel
{
    [Required]
    public string username { get; set; }

    [Required]
    public string password { get; set; }
    
    public string? nome { get; set; }
    public string? cognome { get; set; }
    
    public string ente { get; set; }

    public List<SelectListItem> ListaRuoli { get; set; }

    [Required]
    public string ruolo { get; set; }

}