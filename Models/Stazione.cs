using System.ComponentModel.DataAnnotations;
namespace GNSShare_client.Models;

public class StazioneModel
{
    [Required]
    public string codice { get; set; }
    public string nome { get; set; }
    
    [Required]
    [RegularExpression(@"^-?[0-9]*(?:[\.,][0-9]*)?$")]
    public string latitudine { get; set; }
    [Required]
    [RegularExpression(@"^-?[0-9]*(?:[\.,][0-9]*)?$")]
    public string longitudine { get; set; }
    [Required]
    [RegularExpression(@"^-?[0-9]*(?:[\.,][0-9]*)?$")]
    public string altitudine { get; set; }
    
    public string rete { get; set; }
    public string tipo { get; set; }

    public bool gps { get; set; }
    public bool glonass { get; set; }
    public bool galileo { get; set; }
}