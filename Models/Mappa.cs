using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;
namespace GNSShare_client.Models;

public class MappaModel
{
    public Dictionary<string, client.Stazione>? ListaStazioni { get; set; }
    public string stazioneScelta { get; set; }
    public client.Stazione _stazioneScelta { get; set; }

    //public List<SelectListItem> ListaFile { get; set; }
    public string fileScelto { get; set; }
}