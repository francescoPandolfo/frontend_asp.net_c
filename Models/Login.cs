using System.ComponentModel.DataAnnotations;
using System.Web;

namespace GNSShare_client.Models;

public class LoginModel
{
    [Required]
    public string username { get; set; }
    [Required]
    public string password { get; set; }  
    
}